import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    number1: 1220,
    number2: 22,
  },
  mutations: {
    sum(state) {
      state.number1 = state.number2 * 2;
    },
    const(state) {
      state.number2 = 1000;
    },
    setNewValue(state, data) {
      state[data.num] = data.v;
    },
    incrementOrDecrement(state, data) {
      state[data.num] = data.direction
        ? Number(state[data.num]) + data.step : Number(state[data.num]) - data.step;
    },
  },
  actions: {
    sum(context) {
      context.commit('sum');
    },
    const(context) {
      context.commit('const');
    },
    setNewValue(context, data) {
      context.commit('setNewValue', data);
    },
    incrementOrDecrement(context, data) {
      context.commit('incrementOrDecrement', data);
    },
  },
  getters: {
    number1(state) {
      return state.number1;
    },
    number2(state) {
      return state.number2;
    },
  },
});
