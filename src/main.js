import Vue from 'vue';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

Vue.filter('convertToCurrency', (value) => {
  if (value === null) return '';
  const formatter = new Intl.NumberFormat('ru-RU');
  return formatter.format(value);
});

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
